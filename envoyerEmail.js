var nodemailer = require('nodemailer');
var smtpPool = require('nodemailer-smtp-pool');
var variable_env = require('./configuration.json');

//***************************************************************************************************
//  Envoi d'un email a un destinataire
//  @config         : Donnnees du destinataire.
//  @liste_ajout    : liste des nouvelles installations.
//***************************************************************************************************
envoyerEmail = function (config,liste_ajout){

  //Configuration du compte source.
  var transporter = nodemailer.createTransport(smtpPool({
    service:variable_env.courriel_service,
    auth:{
      user : variable_env.courriel_gmail,
      pass : variable_env.courriel_password
    }
  }));
  //Configuration du destinataire.
  if(config.destinataire.id!=undefined){
    var mailOptions = {
      from:variable_env.courriel_gmail,
      to: config.destinataire.email,
      subject:config.destinataire.name + " Voici la liste des ajouts",
      text:"liste mis a jour de la ville: " + "\n" + liste_ajout +"\n" + " Cliquez sur ce lien pour vous désabonner "+" http://localhost:3000/desabonner?id="+config.destinataire.id
    }
  }else{
    var mailOptions = {
      from:variable_env.courriel_gmail,
      to: config.destinataire.email,
      subject:config.destinataire.name + " Voici la liste des ajouts",
      text:"Liste mis à jour de la ville : " + "\n" + liste_ajout +"\n"
    }
  }
  //Envoi du mail au destinataire.
  transporter.sendMail(mailOptions,function(error,info){
    if(error){
      console.log(error);
    }else{
      console.log("Envoi du message au destinataire : "+ config.destinataire.name +" effectue avec succes ... ");
    }
  });
}

module.exports = envoyerEmail;
