var tab_nom_arr = [];
var liste_ajout = "";
var liste_ajout_nom_arr = "";
var fichier_destinataire = "destinataire.yml";
//Declaration des dependances.
var fs              = require("fs");
var mongo           = require("mongodb");
var http            = require("http");
const httpRequest   = require('request');
var parser          = require("xml2json");
var csv             = require("csvtojson");
const yaml          = require('js-yaml');
var Twitter         = require('twitter');
var envoyerEmail    = require('./envoyerEmail.js');
var foundNewData    = require('./foundNewData.js');
var variable_env    = require('./configuration.json');
//URL Site de la Ville et connection a la BD.
var uri = variable_env.url_base_de_donnees;
var url_liste_piscine = variable_env.url_liste_piscine;
var url_liste_patinoire = variable_env.url_liste_patinoire;
var url_liste_aire_jeu = variable_env.url_liste_aire_jeu;

// Credentials API Twitter.
var client = new Twitter({
  consumer_key: variable_env.consumer_key,
  consumer_secret: variable_env.consumer_secret,
  access_token_key: variable_env.access_token_key,
  access_token_secret: variable_env.access_token_secret
});
//***************************************************************************************************
//  Step 1 : Recuperation des donnees a partir des URL du site web de la ville de Montreal.
//  Step 2 : Parsing des donnnees.
//  Step 3 : Connection a la base de donnees MLAB.
//  Step 4 : Comparaison des donnees pour detection ajout.
//  Step 5 : Insertion dans la base de donnees.
//      Si nouvelles installations detectees:
//          a.Envoi d'un mail au destinataire
//          b.Post du Tweet
//          c.Envoi d'un mail aux utilisateurs inscrits
//***************************************************************************************************
getData = function(){
  var liste_piscine ;
  var liste_patinoire;
  var liste_aire_jeu;
  // Recuperation des donnees a partir des URL de la ville.
  httpRequest({
    url: url_liste_piscine,
    method: "GET",
  }, function (error, resp, body) {
    liste_piscine = body;
    httpRequest({
      url: url_liste_patinoire,
      method: "GET",
    }, function (error, resp, body) {
      liste_patinoire = body;
      httpRequest({
        url: url_liste_aire_jeu,
        method: "GET",
      }, function (error, resp, body) {
        liste_aire_jeu = body;

        // Parsing CSV to JSON.
        csv()
        .fromString(liste_piscine)
        .on("end_parsed",function(jsonArrayObj){

          var liste_piscine_transform = jsonArrayObj;
          // Parsing XML to JSON.
          liste_patinoire_transform  = parser.toJson(liste_patinoire+"");
          liste_aire_jeu_transform   = parser.toJson(liste_aire_jeu+"");
          liste_aire_jeu_transform_parse = JSON.parse(liste_aire_jeu_transform);
          liste_patinoire_transform_parse = JSON.parse(liste_patinoire_transform);
          try{
            //Connection a la BD.
            mongo.MongoClient.connect(uri, function(err,db){
              if(err) throw err;

              var collection_patinoires   = db.collection("Patinoires");
              var collection_piscines     = db.collection("Piscines");
              var collection_airejeu      = db.collection("Airejeu");
              var collection_compte       = db.collection("Comptes");
              var tab = [];
              collection_patinoires.find().toArray(function(err,result){
                var nom_collection = "Patinoires";
                var json_patinoire = JSON.stringify(liste_patinoire_transform_parse.patinoires.patinoire);
                if(result.length!=0){
                  foundNewData(json_patinoire,nom_collection,result,tab,liste_patinoire_transform_parse,db)
                }
                collection_piscines.find().toArray(function(err,result){
                  var nom_collection = "Piscines";
                  var json_piscine = JSON.stringify(liste_piscine_transform);
                  if(result.length!=0){
                    foundNewData(json_piscine,nom_collection,result,tab,liste_piscine_transform,db);
                  }
                  collection_airejeu.find().toArray(function(err,result){
                    var nom_collection = "Airejeu";
                    var json_airejeu = JSON.stringify(liste_aire_jeu_transform_parse.glissades.glissade);
                    if(result.length!=0){
                      foundNewData(json_airejeu,nom_collection,result,tab,liste_aire_jeu_transform_parse,db);
                    }

                    // Remplissage du tableau de donnnes.
                    for(var i = 0; i<tab.length;i++){
                      if( (i%2) === 0){
                        liste_ajout = liste_ajout + tab[i] + "\n" ;
                      }else{
                        tab_nom_arr.push(tab[i]);
                      }
                    }

                    const config = yaml.safeLoad(fs.readFileSync(fichier_destinataire, 'utf8'));
                    if(tab.length!=0){
                      // Envoyer Email au destinataire du fichier  destinataire.yml
                      envoyerEmail(config,liste_ajout);
                      // Poster un Tweet
                      var tweet_liste = liste_ajout
                      var temp = liste_ajout;
                      if(tweet_liste.length>280){
                        temp = tweet_liste.substring(0,278);
                      }
                      client.post('statuses/update',{status:temp},function(error,tweet,response){
                        if(error) {
                          console.log(error);
                        }else{
                          console.log("Post du Tweet avec succes...");
                        }
                      });
                      // Envoyer Email aux abonnes suivant un arrondissment.
                      var cursor_compte =collection_compte.find({ liste_arrondissement:{$in: tab_nom_arr }  });
                      cursor_compte.toArray(function (err, liste_compte) {
                        for(var i = 0;i<liste_compte.length;i++){
                          var liste_nom_arr = liste_compte[i].liste_arrondissement;
                          liste_ajout ="";
                          for(var a = 0;a<liste_nom_arr.length;a++){
                            for(var b = 1;b<tab.length;b=b+2){
                              if(liste_nom_arr[a]===tab[b]){
                                liste_ajout = liste_ajout + tab[b-1] + "\n" ;
                              }
                            }
                          }
                          var config = {"destinataire":
                          { "email":liste_compte[i].adresse_email, "name":liste_compte[i].nom,"id":liste_compte[i]._id}
                        };
                        if(liste_ajout!=""){
                          envoyerEmail( config,liste_ajout);
                        }
                      }
                    });
                  }
                  collection_patinoires.remove();
                  collection_piscines.remove();
                  collection_airejeu.remove();
                  console.log("Insertion des donnnees dans la BD ...");
                  collection_patinoires.insert(liste_patinoire_transform_parse.patinoires.patinoire);
                  collection_piscines.insert(liste_piscine_transform);
                  collection_airejeu.insert(liste_aire_jeu_transform_parse.glissades.glissade);
                  console.log("Running ...");
                });
              })
            });
          });
        }catch (err) {
          console.error("an error occured", err);
          const httpResponse = {
            statusCode : 500,
            headers : {"content-type":"application/json"},
            body : JSON.stringify({})
          };
        }
      });
    });
  });
});
}

module.exports.getData = getData;
