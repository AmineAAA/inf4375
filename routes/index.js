var express = require('express');
var router = express.Router();
var http = require('http');
var port = process.env.PORT || 3000;
var bodyParser = require('body-parser');
var concat = require('array-concat')
var renameKeys = require('rename-keys');
var unique = require('array-unique');
const sortBy = require('sort-array')
var js2xmlparser = require("js2xmlparser");
var jsonConcat = require("json-concat");
var fichier = require('object-to-xml')
var ObjectId = require('mongodb').ObjectID;
var json2csv = require('json2csv');
var fs = require('fs');
var Validator = require('jsonschema').Validator;
var raml2html = require('raml2html');

var uri = 'mongodb://amine_db:amine_db@ds149495.mlab.com:49495/inf4375';
var mongo = require("mongodb");

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'SAS-MTL', ndf: 'Automne 2017'  });
});

/* GET documentation page. */
router.get('/doc', function(req, res, next) {
  theme = raml2html.getConfigForTheme();
  raml2html.render('./ramlRest.raml', theme)
  .then(function(html){
    res.send(html)
  })
});

/* GET desabonnement page. */
router.get('/desabonner', function(req, res, next) {
  res.render('desabonner', { title: 'Se desabonner', ndf: 'Automne 2017'  });
});

//***************************************************************************************************
//  Recuperation des installations de la BD en format json a partir d'un parametre dans l'URL
//***************************************************************************************************
router.get('/installations', function(req, res) {
  var nom_Arrondissement = req.param('arrondissement');
  try{
    mongo.connect(uri, function(err,db){
      var collection_patinoires = db.collection("Patinoires");
      var collection_airejeu = db.collection("Airejeu");
      var collection_piscines= db.collection("Piscines");
      var cursor_un = collection_patinoires.find({"arrondissement.nom_arr" : nom_Arrondissement}, {nom:true , _id:false});
      var cursor_deux = collection_airejeu.find({"arrondissement.nom_arr" : nom_Arrondissement}, {nom:true , _id:false});
      var cursor_trois = collection_piscines.find({"ARRONDISSE" : nom_Arrondissement}, {NOM:true , _id:false});
      cursor_un.toArray(function (err, liste_installation_un) {
        cursor_deux.toArray(function (err, liste_installation_deux) {
          cursor_trois.toArray(function (err, liste_installation_trois) {
            var liste_installation_tempo =  concat( liste_installation_deux, liste_installation_trois);
            var liste_installation =  concat( liste_installation_un, liste_installation_tempo);
            res.send({liste_installation});
            db.close();
          });
        });
      });
    });
  }catch (err) {
    console.error("an error occured", err);
    const httpResponse = {
      statusCode : 500,
      headers : {"content-type":"application/json"},
      body : JSON.stringify({})
    };
  }
});

//***************************************************************************************************
//  Recuperation de toutes installations de la BD en format json
//***************************************************************************************************
router.get('/installations/global', function(req, res) {
  try{
    mongo.connect(uri, function(err,db){
      var collection_patinoires = db.collection("Patinoires");
      var collection_airejeu = db.collection("Airejeu");
      var collection_piscines= db.collection("Piscines");
      var cursor_un = collection_patinoires.find();
      var cursor_deux = collection_airejeu.find();
      var cursor_trois = collection_piscines.find();
      cursor_un.toArray(function (err, liste_installation_un) {
        cursor_deux.toArray(function (err, liste_installation_deux) {
          cursor_trois.toArray(function (err, liste_installation_trois) {
            var liste_installation_tempo =  concat( liste_installation_deux, liste_installation_trois);
            var liste_installation =  concat( liste_installation_un, liste_installation_tempo);
            res.send({liste_installation});
            db.close();
          });
        });
      });
    });
  }catch (err) {
    console.error("an error occured", err);
    const httpResponse = {
      statusCode : 500,
      headers : {"content-type":"application/json"},
      body : JSON.stringify({})
    };
  }
});

//***************************************************************************************************
//  Recuperation des installations en mauvaise condition en format json
//***************************************************************************************************
router.get('/installations/condition/mauvaise', function(req, res) {
  try{
    mongo.connect(uri, function(err,db){
      var collection_patinoires = db.collection("Patinoires");
      var collection_airejeu = db.collection("Airejeu");
      var collection_piscines= db.collection("Piscines");
      var cursor_un = collection_patinoires.find({"condition" : "Mauvaise"});
      var cursor_deux = collection_airejeu.find({"condition" : "Mauvaise"});
      cursor_un.toArray(function (err, liste_installation_un) {
        cursor_deux.toArray(function (err, liste_installation_deux) {
          var liste_mauvaise_condition =  concat( liste_installation_deux, liste_installation_un);
          sortBy(liste_mauvaise_condition, 'nom');
          res.send({liste_mauvaise_condition});
          db.close();
        });
      });
    });
  }catch (err) {
    console.error("an error occured", err);
    const httpResponse = {
      statusCode : 500,
      headers : {"content-type":"application/json"},
      body : JSON.stringify({})
    };
  }
});

//***************************************************************************************************
//  Recuperation des installations en mauvaise condition en format xml
//***************************************************************************************************
router.get('/installations/condition/mauvaise/xml', function(req, res) {
  try{
    mongo.connect(uri, function(err,db){
      var collection_patinoires = db.collection("Patinoires");
      var collection_airejeu = db.collection("Airejeu");
      var collection_piscines= db.collection("Piscines");
      var cursor_un = collection_patinoires.find({"condition" : "Mauvaise"},{_id:false});
      var cursor_deux = collection_airejeu.find({"condition" : "Mauvaise"},{_id:false});
      cursor_un.toArray(function (err, liste_installation_un) {
        cursor_deux.toArray(function (err, liste_installation_deux) {
          var installation_mauvaise_condition =  concat( liste_installation_deux, liste_installation_un);
          sortBy(installation_mauvaise_condition, 'nom');
          var liste_mauvaise_condition = {installation_mauvaise_condition}
          res.setHeader('content-type','application/xml')
          res.send(fichier({
            '?xml version ="1.0" encoding="utf-8"?':null,liste_mauvaise_condition
          }));
          db.close();
        });
      });
    });
  }catch (err) {
    console.error("an error occured", err);
    const httpResponse = {
      statusCode : 500,
      headers : {"content-type":"application/json"},
      body : JSON.stringify({})
    };
  }
});

//***************************************************************************************************
//  Recuperation des installations en mauvaise condition en format csv
//***************************************************************************************************
router.get('/installations/condition/mauvaise/csv', function(req, res) {
  try{
    mongo.connect(uri, function(err,db){
      var fields = ['_id','nom', 'arrondissement.nom_arr','arrondissement.cle', 'arrondissement.date_maj','ouvert','deblaye', 'condition'];
      var fieldNames = ['_id','nom', 'nom_arr arrondissement','cle arrondissement', 'date_maj arrondissement','ouvert','deblaye', 'condition'];
      var collection_patinoires = db.collection("Patinoires");
      var collection_airejeu = db.collection("Airejeu");
      var collection_piscines= db.collection("Piscines");
      var cursor_un = collection_patinoires.find({"condition" : "Mauvaise"});
      var cursor_deux = collection_airejeu.find({"condition" : "Mauvaise"});
      cursor_un.toArray(function (err, liste_installation_un) {
        cursor_deux.toArray(function (err, liste_installation_deux) {
          var installation_mauvaise_condition =  concat( liste_installation_deux, liste_installation_un);
          sortBy(installation_mauvaise_condition, 'nom');
          var opts = {
            data: installation_mauvaise_condition,
            fields: fields,
            fieldNames: fieldNames,
            quotes: ''
          };
          var csv = json2csv(opts);
          res.setHeader('content-type','text/csv')
          res.send(csv);
          db.close();
        });
      });
    });
  }catch (err) {
    console.error("an error occured", err);
    const httpResponse = {
      statusCode : 500,
      headers : {"content-type":"application/json"},
      body : JSON.stringify({})
    };
  }
});

//***************************************************************************************************
//  Recuperation des infos d'une installation.
//***************************************************************************************************
router.get('/infos', function(req, res) {
  var nom_installation = req.param('installation');
  try{
    mongo.connect(uri, function(err,db){
      var collection_patinoires = db.collection("Patinoires");
      var collection_airejeu = db.collection("Airejeu");
      var collection_piscines= db.collection("Piscines");
      var cursor_un = collection_patinoires.find({"nom" : nom_installation}, {_id:false});
      var cursor_deux = collection_airejeu.find({"nom" : nom_installation}, {_id:false});
      var cursor_trois = collection_piscines.find({"NOM" : nom_installation}, {_id:false});
      cursor_un.toArray(function (err, liste_installation_un) {
        cursor_deux.toArray(function (err, liste_installation_deux) {
          cursor_trois.toArray(function (err, liste_installation_trois) {
            var liste_installation_tempo =  concat( liste_installation_deux, liste_installation_trois);
            var liste_installation =  concat( liste_installation_un, liste_installation_tempo);
            res.send({liste_installation});
            db.close();
          });
        });
      });
    });
  }catch (err) {
    console.error("an error occured", err);
    const httpResponse = {
      statusCode : 500,
      headers : {"content-type":"application/json"},
      body : JSON.stringify({})
    };
  }
});

//***************************************************************************************************
//  Modification d'une glissade a partir de son id et de sa condition.
//  @param  req : JSON dans lequel est inclus l'id MongoDb du la glissade (_id) et la condition a modifier
//  @return  description de la reponse de la requete.
//***************************************************************************************************
router.patch('/modifierglissade', function(req, res) {
  var httpResponse= null
  var v = new Validator();
  var schema = {
    "type": "object",
    "required": true,
    "properties": {
      "id": {
        "type": "string",
        "required": true
      },
      "condition": {
        "type": "string",
        "required": true
      }
    }
  }

  var result =v.validate(req.body, schema);
  var erreur_schema = result.errors.length;

  if(erreur_schema===0){
    var des;
    try{
      mongo.connect(uri, function(err,db){
        var collection_airejeu = db.collection("Airejeu");
        collection_airejeu.update({"_id": ObjectId(req.body.id)}, {$set: {"condition": req.body.condition}}, function (err, result) {
          var obj = JSON.parse(result);
          if(obj.nModified === 1){
            des = "element modifie avec succes"
          }else{
            des ="element introuvable"
          }
          httpResponse = {
            status:200,
            headers:{
              "x-custom-header":"My Header Value"
            },
            body:{"status":"success","data":{"description":des}}
          }
          res.send(httpResponse)
          db.close();
        });
      });
    }catch (err) {
      console.error("an error occured", err);
      const httpResponse = {
        statusCode : 500,
        headers : {"content-type":"application/json"},
        body : JSON.stringify({})
      };
    }
  }else {
    des = "Votre json ne respecte pas le schema"
    httpResponse = {
      status:400,
      headers:{
        "x-custom-header":"My Header Value"
      },
      body:{"status":"fail","data":{"description":"Input does not match schema", "error":result.errors}}
    }
    res.send(httpResponse)
  }
});

//***************************************************************************************************
//  Suppression d'une glissade a partir de son id.
//  @param  req : JSON dans lequel est inclus l'id MongoDb du la glissade (_id)
//  @return  description de la reponse de la requete.
//***************************************************************************************************
router.delete('/supprimerglissade', function(req, res) {
  var httpResponse= null
  var v = new Validator();
  var schema = {
    "type": "object",
    "required": true,
    "properties": {
      "id": {
        "type": "string",
        "required": true
      }
    }
  }

  var result =v.validate(req.body, schema);
  var erreur_schema = result.errors.length;
  if(erreur_schema===0){
    var des;
    try{
      mongo.connect(uri, function(err,db){
        var collection_airejeu = db.collection("Airejeu");
        collection_airejeu.deleteOne({"_id": ObjectId(req.body.id)}, function (err, result) {
          var obj = JSON.parse(result);
          if(result.deletedCount > 0){
            des = "glissade supprime avec succes"
          }else{
            des ="glissade introuvable"
          }
          httpResponse = {
            status:200,
            headers:{
              "x-custom-header":"My Header Value"
            },
            body:{"status":"success","data":{"description":des}}
          }
          res.send(httpResponse)
          db.close();
        });
      });
    }catch (err) {
      console.error("an error occured", err);
      const httpResponse = {
        statusCode : 500,
        headers : {"content-type":"application/json"},
        body : JSON.stringify({})
      };
    }
  } else {
    httpResponse = {
      status:400,
      headers:{
        "x-custom-header":"My Header Value"
      },
      body:{"status":"fail","data":{"description":"Input does not match schema", "error":result.errors}}
    }
    res.send(httpResponse)
  }
});

//***************************************************************************************************
//  Creation du profil du client.
//  @param  req : JSON dans lequel sont inclus le nom, l'adresse_email et sa liste d'arrondissement.
//  @return description de la reponse de la requete.
//***************************************************************************************************
router.post('/creerprofil', function(req, res) {
  var httpResponse= null
  var v = new Validator();
  var schema = {
    "type": "object",
    "required": true,
    "properties": {
      "nom": {
        "type": "string",
        "required": true
      },
      "adresse_email": {
        "type": "string",
        "required": true
      },
      "liste_arrondissement": {
        "type": "array",
        "items": {"type": "string"},
        "required": true
      }
    }
  }

  var result =v.validate(req.body, schema);
  var erreur_schema = result.errors.length;
  if(erreur_schema===0){
    var des;
    var exist;
    try{
      mongo.connect(uri, function(err,db){
        var collection_compte= db.collection("Comptes");
        var valid = collection_compte.find({"adresse_email":req.body.adresse_email})
        valid.toArray(function (err, find) {
          exist = find.length;
          if(exist===0||exist===undefined){
            collection_compte.insert(req.body, function (err, result) {
              console.log(result);
              if(result.insertedCount > 0){
                des = "--Votre profil a été rajouté avec succés--"
              }else{
                des ="--Ajout impossible--"
              }
              httpResponse = {
                status:201,
                headers:{
                  "x-custom-header":"My Header Value"
                },
                body:{"status":"success","data":{"description":des}}
              }
              res.send(httpResponse)
              db.close();
            });
          }else{
            des ='--Création impossible compte existant--'
            httpResponse = {
              status:200,
              headers:{
                "x-custom-header":"My Header Value"
              },
              body:{"status":"success","data":{"description":des}}
            }
            res.send(httpResponse)
            db.close();
          }
        })
      });
    }catch (err) {
      console.error("an error occured", err);
      const httpResponse = {
        statusCode : 500,
        headers : {"content-type":"application/json"},
        body : JSON.stringify({})
      };
    }
  } else {
    httpResponse = {
      status:400,
      headers:{
        "x-custom-header":"My Header Value"
      },
      body:{"status":"fail","data":{"description":"Input does not match schema", "error":result.errors}}
    }
    res.send(httpResponse)
  }
});

//***************************************************************************************************
//  Desabonnement du client a partir de son id.
//  @param  req : JSON dans lequel est inclus l'id MongoDb du client (_id)
//  @return  description de la reponse de la requete.
//***************************************************************************************************
router.delete('/desabonner', function(req, res) {
  try{
    mongo.connect(uri, function(err,db){
      var collection_compte = db.collection("Comptes");
      collection_compte.deleteOne({"_id": ObjectId(req.body.id)}, function (err, result) {
        if(result.deletedCount > 0){
          des = "Vous etes désabonné !"
        }else{
          des ="Désabonnement impossible !"
        }
        httpResponse = {
          status:200,
          headers:{
            "x-custom-header":"My Header Value"
          },
          body:{"status":"success","data":{"description":des}}
        }
        res.send(httpResponse)
        db.close();
      });
    });
  }catch (err) {
    console.error("an error occured", err);
    const httpResponse = {
      statusCode : 500,
      headers : {"content-type":"application/json"},
      body : JSON.stringify({})
    };
  }
});

module.exports = router;
