//***************************************************************************************************
//  Execution de la mise a jour automatique des donnnees.
//  Frequence minuit :  '0 0 * * *'
//***************************************************************************************************

var cron = require("node-cron");
var cronJob = () => {
  cron.schedule('0 0 * * *',function(){
    console.log("Mise à jour de la base de données");
    getData();
  });
}
module.exports.cronJob = cronJob;
