var fs = require("fs");
var tab_filtre;
var jf = require('jsonfile');
var mongo           = require("mongodb");
var http            = require("http");
const httpRequest   = require('request');
var variable_env = require('./configuration.json');
var uri = variable_env.url_base_de_donnees;

//***************************************************************************************************
//  Recuperer les donnees , compare  avec les donnees existantes et dresse
//  une liste des noms des nouvelles installations et des noms des arrondissements dans un tableau.
//
//  @liste_installation      : liste d'installations venant de la ville.
//  @nom_collection          : nom de la collection.
//  @result                  : tableau des nouvelles installations detectees.
//  @liste_json              : json lie a une collection.
//  @db                      : base de donnees.
//***************************************************************************************************

foundNewData = function (liste_installation,nom_collection,result,tab,liste_json,db) {

  var nouveau = JSON.parse(liste_installation);
  var ancien  = result;
  if(ancien.length!=nouveau.length){
    console.log("Detection mis a jour ...");
    if(nouveau[0].nom!=undefined){
      for(var i = 0; i<nouveau.length;i++){
        var present = false;
        for(var j = 0 ; j <ancien.length;j++){
          if((nouveau[i].nom == ancien[j].nom)){
            present= true;
            break;
          }
        }
        if(!present){
          tab.push(nouveau[i].nom);
          tab.push(nouveau[i].arrondissement.nom_arr);
        }
      }
    }else{
      for(var i = 0; i<nouveau.length;i++){
        var present = false;
        for(var j = 0 ; j <ancien.length;j++){
          if((nouveau[i].NOM == ancien[j].NOM)){
            present= true;
            break;
          }
        }
        if(!present){
          tab.push(nouveau[i].NOM);
          tab.push(nouveau[i].ARRONDISSE);
        }
      }
    }
    tab_filtre = tab.filter(function(elem, index, self) {
      return index == self.indexOf(elem);
    })
  }
}

module.exports = foundNewData;
