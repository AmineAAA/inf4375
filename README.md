# SAS-MTL Service d'Activitées Sportives de Montréal

## Description

  SAS-MTL est un projet qui consiste à mettre en place une interface web capable de fournir à des utilisateurs l'accés en temps réel aux differentes informations concernant les activités sportives la ville de Montréal.
  Ce projet est développé dans le cadre du cours INF4375 Paradigmes des échanges Internet de la session d'automne 2017 au sein de l'UNIVERSITE DU QUEBEC A MONTREAL(UQAM) donné par Jacques Berger.

## Auteur

  Prenom et Nom  :  Amine Oufini
  Code Permanent :  OUFA17119004
  Courriel : oufini.amine@hotmail.fr

  Prenom et Nom  :  Douglas Kouokam Kouam
  Code Permanent :  KOUD16019209
  Courriel : dapoual@hotmail.com

## Introduction aux fonctionnalitées du projet

SAS-MTL est un projet composé de plusieurs fonctionnalités. Chacune d'entre elles comporte un certain nombre de points d'expérience. Concernant notre version, celle-ci comporte 185 XP. Toutes les fonctionnalités ont été developpées sauf D5 et D6.

## Note

Veuillez ne pas utiliser le réseau Eduroam lors du test de cette application. Le fichier CSV est bloqué par ce réseau.
Merci!

## Fonctionnement

Première étape avant les tests:

1. Dézipper l'archive du projet.
2. Ouvrir une ligne de commande et se dériger vers le dossier du projet, ensuite dézipper.
3. Faire la commande npm install pour telecharger les dependances.
4. Pour lancer le projet:  node bin/www

Deuxième étape, les tests:

A1:

1. Ouvrir mlab.com
2. Se connecter
3. Selectionner la base de données INF4375 et verifier si les collections sont bien inexistantes. Si elles existent, supprimer les collections.

0. OU

1. Mettre l'url de votre base de donnees dans le fichier configuration.json
2. Se connecter a votre base de donnees
3. Selectionner votre base de données  et verifier si les collections sont bien inexistantes. Si elles existent, supprimer les collections.


4. Aller à la ligne de commande et faire: node bin/www

6. Pour confirmer que trois listes de données ont été obtenues et sauvegradées dans une base de données mongodb, aller sur la BD. Chacune des groupes de donnees sera enregistrée dans une collection spécifique.

7. Fermer le serveur avec CONTROL+C et redemarrer a partir de l'étape 4 pour verifier qu'il n'y a pas de duplication de données.

A2:

Cette fonctionnalitée va prendre beaucoup de temps pour se faire tester, car comme l'énoncé le dit, l'importation de données du point A1 est faite automatiquement chaque jour à minuit. Notre application respecte ce point donc pour pouvoir la tester, il  faut modifier une seule ligne dans le fichier cronJob.js

1. CONTROL+C pour arreter l'application.
2. Ouvrir le fichier cronJob.js avec un editeur de text.
3. Changer la frequence du cronJb
4. Sauvegarder et lancer la commande node bin/www
5. Attendre que les données se rechargent selon la fréquence choisit.

A3:

Le système écoute les requètes sur le port 3000. Il existe plusieurs facons de le verifier.

1. Lancer le système avec la commande node bin/www
2. Aller à l'adresse http://localhost:3000/ Le 3000 indique le port utilisé.

La route « /doc » fait apparaître la documentation de tous les services REST.

1. Lancer le système avec la commande node bin/www
2. Aller à la route /doc

A4:

1- Aller sur la route /installations?arrondissement= , un objet JSON vide apparaitra.
2- En rajoutant une valeur au paramètre arrondissement, on obtient un JSONObject contenant un tableau avec toutes les installations existantes dans cette arrondissement.
Exemple: /installations?arrondissement=LaSalle

A5:

1- Aller sur la route /
2- Un formulaire demandant le nom d'arrondissement apparaitra.
3- Inscrire un arrondissement de la même manière que c'est écrit dans l'objet JSON.
exemple: LaSalle
4- Un tableau s'affiche avec les noms d'installations que contient l'arrondissement

A6:

1- Aller à la route /
2- Une liste déroulante demandant de choisir une installation apparaitra.
3- Choisir l'installation.
4- Un tableau s'affiche avec toutes les informations sur cette installation.

B1:

1- Aller à la base de données.
2- Supprimer une ou plusieurs installations.
3- Modifier le courriel ou le nom du destinataire dans le fichier destinataire.yml
4- Lancer le serveur avec la commande node bin/www et observer le resultat dans votre courriel.

B2:

1- Aller à la base de données.
2- Supprimer une ou plusieurs intallations.
3- Modifier les API_KEYS du compte Tweeter dans le fichier configuration.json
4- Lancer le serveur avec la commande node bin/www et observer le resultat dans votre compte Tweeter.

C1:

1- Aller à la route /installations/condition/mauvaise
2- Un objet JSON contenant un tableau du nom liste_mauvaise_condition sera affiché avec la liste des installations en mauvaise condition.
Note: Veuillez vérifier qu'il existe bien des installations en mauvaise condition avant de tester cette fonctionnalité.

C2:

Aller à la route /installations/condition/mauvaise/xml

C3:

Aller a la route /installations/condition/mauvaise/csv

Note: Si vous utilisez Chrome, il se peut que ce service soit telechargé en forme de fichier csv. Ce fichier contiendra en effet les installations en mauvaise conditions au format CSV.

D1:

1- Ouvrir Postman
2- Choisir PATCH et mettre l'url: http://localhost:3000/modifierglissade
3- Lancer node bin/www
4- Aller sur mongodb et selectionner le OID d'une glissade (Se trouve dans la collection Airejeu).

Envoyer :
{
    "id": "coller le OID ici",
    "condition": "Bonne"
}

Vous devriez avoir ce message:
{
    "status": 200,
    "headers": {
        "x-custom-header": "My Header Value"
    },
    "body": {
        "status": "success",
        "data": {
            "description": "element modifie avec succes"
        }
    }
}

Vérfier dans la base de données que la condition a bien changé.

D2:

1- Ouvrir Postman
2- Choisir DELETE et mettre l'url: http://localhost:3000/supprimerglissade
3- Lancer node bin/www
4- Aller sur mongodb et selectionner le OID d'une glissade (collection Airejeu).
Note: rendu a cette etape ne pas redemarrer l'application sinon le OID va changer.
5- Envoyer :
{
    "id": "coller le OID ici"
}

Vous devriez avoir ce message:
{
    "status": 200,
    "headers": {
        "x-custom-header": "My Header Value"
    },
    "body": {
        "status": "success",
        "data": {
            "description": "glissade supprime avec succes"
        }
    }
}
6- Vérfier dans la base de données que l'installation a bien été supprimée.

E1:

1- Aller à la route /
2- Une section demande "Veuillez entrer vos coordonnées et inscrire un ou des arrondissments à suivre"
Inscrire un nom, une adresse courriel ainsi qu'une liste d'intallations à surveiller. Ces installations doivent être séparées par des virgules.
3- Aller dans la base de données, une collection Comptes contiendra l'objet JSON contenant les informations du compte crée.


E2:

1- Aller à la route /

E3:

1- Aller à la base de données.
2- Supprimer une installation d'un des arrondissements que vous suivez dans le compte crée plus haut.
3- Lancer le serveur avec la commande node bin/www
4- Aller à votre boite courriel (avec laquelle vous avez créer votre compte) pour voir la ou les installations.

E4:

1- À l'étape 5 de E3 le courriel contiendra un lien pour se désabonner.
2- Appuyer sur le lien.
3- Choisir CONFIRMER sur la route /desabonner
4- Aller à la collection Comptes sur la BD. Le compte n'existe plus.

ou

1- Ouvrir Postman
2- Choisir DELETE et mettre l'url: http://localhost:3000/desabonner
3- Lancer node bin/www
4- Aller sur mongodb et selectionner le OID d'un client (collection Comptes).
Note: rendu a cette etape ne pas redemarrer l'application sinon le OID va changer.
5- Envoyer :
{
    "id": "coller le OID ici"
}

Vous devriez avoir ce message:
{
    "status": 200,
    "headers": {
        "x-custom-header": "My Header Value"
    },
    "body": {
        "status": "success",
        "data": {
            "description": "desabonnement avec succes"
        }
    }
}
6- Vérfier dans la base de donnee que l'installation a bien été supprimée.

F1:

Aller sur : https://tranquil-island-61486.herokuapp.com/


##Pour cloner le projet à partir de bitbucket:

git clone https://AmineAAA@bitbucket.org/AmineAAA/inf4375.git

## Statut

Le projet a été completé avec 185 XP.
